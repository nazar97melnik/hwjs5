"use strict";
// 1. Опишіть своїми словами, що таке метод об'єкту
// метод об'єкту - це функція записана всередині об'єкту і призначенна лише для цього об'єкту
// 2. Який тип даних може мати значення властивості об'єкта?
// абсолютно будь-який , навіть може бути також об'єктом
// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// кожен об'єкт містить посилання на себе , через це ми не можемо зклонувати об'єкт звичайним присвоєнням, тому що тоді ми просто скопіюємо посилання на цей об'єкт.

const newUser={
    getLogin(){
        return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
    },
};
function createNewUser(){
    newUser.firstName = prompt("Enter your name");
    newUser.lastName=prompt("Enter your last name");
    return newUser;
};

console.log(createNewUser());
console.log(newUser.getLogin());
